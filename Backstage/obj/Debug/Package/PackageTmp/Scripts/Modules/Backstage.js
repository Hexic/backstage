﻿Namespaces("Backstage", {
    PreventModal: false,
    boot: function () {
        $(".dropdown-menu > li > a").mouseover(function () {
            $(this).find("i").addClass("icon-white");
        }).mouseout(function () {
            $(this).find("i").removeClass("icon-white");
        });
    },
    postJSON: function (url, data, callback, error) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            success: function (data) {
                if (typeof (callback) === "function") {
                    data = new JsonObject(data);
                    callback(data);
                } else return;
            },
            error: error ||
            function () { },
            cache: false,
            async: false
        });
    }
});

function JsonObject(obj) {
    for (var o in obj) { this[o] = obj[o]; }
    return this;
}

JsonObject.prototype.find = function (p, v) {
    if (Namespaces.exists("Singularity.Util.JsonSelect")) { return Singularity.Util.JsonSelect(this, p, v); }
    else throw new Error("External namespace Singularity.Util.JsonSelector has not been loaded.");
}

$.stopEvent = function (e) {
    e.stopPropagation();
    e.preventDefault();
}


function _StringFormatInline() {
    var txt = this;
    for (var i = 0; i < arguments.length; i++) {
        var exp = new RegExp('\\{' + (i) + '\\}', 'gm');
        txt = txt.replace(exp, arguments[i]);
    }
    return txt;
}

function _StringFormatStatic() {
    for (var i = 1; i < arguments.length; i++) {
        var exp = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        arguments[0] = arguments[0].replace(exp, arguments[i]);
    }
    return arguments[0];
}

if (!String.prototype.format) {
    String.prototype.format = _StringFormatInline;
}
if (!String.format) {
    String.format = _StringFormatStatic;
}

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };
}


$(document).ready(Backstage.boot);