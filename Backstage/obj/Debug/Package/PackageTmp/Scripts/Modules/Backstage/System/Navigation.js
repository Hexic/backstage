﻿Namespaces("Backstage.System.Navigation",
{
    boot: function () {
        $(window).bind("subjects-loaded", Backstage.System.Navigation.postboot);
        Backstage.Subjects.Loader.load();
    },
    bindLinks: function () {
        $(".container-fluid").find("a").each(function () {
            if ($(this).data("binded") != true) {
                $(this).click(Backstage.System.Navigation.performNavigation);
                $(this).data("binded", true);
            }
        });
    },
    postboot: function () {
        Backstage.System.Navigation.bindLinks();
        Backstage.Subjects.Loader.getDocCount();
        if (Backstage.System.Navigation.isBooting) {
            Backstage.System.Navigation.isBooting = false;
            $(".subjectlist").find("a:first").trigger("click");
            setInterval(Backstage.Subjects.Loader.getDocCount, 60000);
        }
    },
    reloadSubject: function () {
        $(".subjectlist").find("li.active").find("a").trigger("click");
    },
    performNavigation: function (e) {
        if ($(this).attr("href") == undefined) { return; }
        if (!$(this).attr("href").startsWith("Backstage")) { return; }
        $().dropdownClose();
        var target = ($(this).attr("href").toString().replace("Backstage.", ""));
        if (target == "ManageSubjects") window.location.href = "/Project/" + Metadata.SafeName + "/Subjects";
        else if (target == "UploadDocument") {
            if (!Backstage.Subjects.Loader.hasSubjects) {
                $("#uploadDocumentNoSubjects").modal("show");
            } else {
                $("#uploadDocument").modal("show");
                $("#uploadDocument").find("input:first").val(Metadata.SafeName);
            }
        }
        else if (target == "NewSubject") {
            $("#newSubject").modal("show");
        } else if (target == "MoveDocument") {
            var docID = $(this).attr("data-itemid");
            $("#moveDocument").data("documentID", docID);
            $("#moveDocument").modal('show');
            
        } else if (target == "RenameDocument") {

            var docID = $(this).attr("data-itemid");
            var id = $("#document-" + docID).find("td > strong").text();
            $("#renameDocument").find("input:first").val(id);
            $("#renameDocument").data("documentID", docID);
            $("#renameDocument").modal('show');
        } else if (target == "RemoveDocument") {
            var docID = $(this).attr("data-itemid");
            var id = $("#document-" + docID).find("td > strong").text();
            $("#deleteDocument").find("span:first").val(id);
            $("#deleteDocument").data("documentID", docID);
            $("#deleteDocument").modal('show');
        } else {
            if (target == "Home") {
                $.ajax({
                    type: "GET",
                    url: "/Project/" + Metadata.SafeName + "/HomePage",
                    success: function (data) {
                        $("#content").html(data);
                    },
                    error: function () { $("#content").html(Backstage.System.Navigation.failedMessage); },
                    cache: false,
                    async: false
                });
                Backstage.System.Navigation.changeCurrentSubject("home");
                Backstage.System.Navigation.bindLinks();
            } else if (target.startsWith("GetDocumentFromHP")) {
                var docid = target.split('[')[1].replace("]", "");
                // Get Document, ETC...
                window.open(window.location.origin + "/Project/" + Metadata.SafeName + "/Document/" + docid, 'resizable,scrollbars');
                // Reload Page (6)
                Backstage.System.Navigation.reloadSubject();
            } else if (target.startsWith("GetDocument")) {
                var docid = target.split('[')[1].replace("]", "");
                // Get Document, ETC...
                window.open(window.location.origin + "/Project/" + Metadata.SafeName + "/Document/" + docid, 'resizable,scrollbars');
                // ReloadPage(6)
                Backstage.System.Navigation.reloadSubject();
            } else if (target.startsWith("ChangeSubject")) {
                var subject = target.split('[')[1].replace("]", "");
                $.ajax({
                    type: "GET",
                    url: "/Project/" + Metadata.SafeName + "/Subject/" + subject,
                    success: function (data) {
                        $("#content").html(data);
                    },
                    error: function () { $("#content").html(Backstage.System.Navigation.failedMessage); },
                    cache: false,
                    async: false
                });
                Backstage.System.Navigation.changeCurrentSubject(subject);
                Backstage.System.Navigation.bindLinks();
            }
        }
        console.log("performNavigation to " + target);
        $("[rel='tooltip']").tooltip();
        $.stopEvent(e);
    },
    changeCurrentSubject: function (current) {
        Backstage.System.Navigation.currentSubject = current;
        Backstage.System.Navigation.updateCurrentSubject();
    },
    updateCurrentSubject: function () {
        $("li[id^='subject-']").removeClass("active");
        $("li[id='subject-" + Backstage.System.Navigation.currentSubject + "']").addClass("active");
    },
    currentSubject: "home",
    failedMessage: "<h3>Não foi possível completar a solicitação</h3><p>Talvez o servidor esteja enfrentando alguma dificuldade técnica. Tente novamente em instantes.</p>",
    isBooting: true
});
$(document).ready(Backstage.System.Navigation.boot);