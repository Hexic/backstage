﻿Namespaces("Backstage.Subjects.Loader", {
    firstGet: true,
    hasSubjects: false,
    load: function () {
        var id = Metadata.idProjects;
        Backstage.postJSON("/raw/getSubjects", { id: id },
        Backstage.Subjects.Loader.parseloadData,
        Backstage.Subjects.Loader.errorHandler);
    },
    parseloadData: function (data) {
        var side = $(".subjectlist");
        var list = $("select[name='areaid']");
        var selectPatternt = "<option value=\"{0}\">{1}</option>";
        var subjectPattern = "<li id=\"subject-{0}\"><a href=\"Backstage.ChangeSubject[{0}]\"><i class=\"icon-inbox\"></i> {1} <span style=\"display:none\" class=\"label label-info pull-right\"></span></a></li>";
        var homeLink = $("<li id=\"subject-home\"><a href=\"Backstage.Home\"><i class=\"icon-home\"></i> Início</a></li>");
        var headerPattern = "<li class=\"nav-header\">{0}</li>";
        var subjects = [];
        list.children().remove();
        list.append($("<option value=\"\" selected=\"selected\">Selecione</option>"));
        subjects.push();
        if (data.length == 0) {
            subjects.push($("<li><a><i class=\"icon-ban-circle\"></i> Não há assuntos</a></li>"));
            $(window).data("Subjects", []);
            Backstage.Subjects.Loader.hasSubjects = false;
        } else {
            $(window).data("Subjects", data.subjects);
            _.each(data.subjects, function (subject) {
                subjects.push($(subjectPattern.format(subject.idAreas, subject.AreaName)));
                list.append($(selectPatternt.format(subject.idAreas, subject.AreaName)));
            });
            Backstage.Subjects.Loader.hasSubjects = true;
        }
        side.children().remove();
        Backstage.Subjects.Loader.firstGet = true;
        side.append(homeLink);
        side.append($(headerPattern.format("Assuntos")));

        _.each(subjects, function (subject) {
            side.append(subject);
        });
        side.append($("[data-model='navlist-end']").html());
        $(window).trigger("subjects-loaded");
    },
    errorHandler: function () {

    },
    getDocCount: function () {
        $("li[id^='subject-']").each(function (e) {
            var id = $(this).attr("id").split('-')[1];
            var el = $(this);
            if (id != "home") {
                Backstage.postJSON("/raw/getDocCount", { area: id }, function (data) {
                    el.find(".label").text(data.DocsCount)
                    var c = data.DocsCount;
                    el.find(".label").attr("title", (c == 0 ? "Nenhum" : c) + " documento" + (c > 1 ? "s" : "") + " nesse assunto");
                    if (Backstage.Subjects.Loader.firstGet) el.find(".label").fadeIn();
                    el.find(".label").tooltip();
                });
            }
        });
        Backstage.Subjects.Loader.firstGet = false;
    }
});