﻿Namespaces("Backstage.Subjects.Management",
{
    boot: function () {
        $("[data-action]").click(Backstage.Subjects.Management.ActionSwitcher);
        $("#renameSubj_commit").click(Backstage.Subjects.Management.attemptRename);
        $("#removeSubj_commit").click(Backstage.Subjects.Management.attemptRemove);
        $("#removingSubject").on("shown", function () {
            Backstage.postJSON("/raw/removeArea", { areaID: $("#removeSubject").data("subjectID") },
            function (data) {
                if (data.Result) {
                    $("#subject-" + $("#removeSubject").data("subjectID")).remove();
                    $("#removingSubject").modal('hide');
                    if ($("table > tbody").children().length < 1) window.location.reload();
                } else {
                    $("#removingSubject").modal('hide');
                    $("#removingSubjectFailed").modal('show');
                }
            }, function () {
                $("#removingSubject").modal('hide');
                $("#removingSubjectFailed").modal('show');
            });
        });
    },
    ActionSwitcher: function (e) {
        var action = $(this).attr("data-action");
        var id = $(this).attr("data-id");
        if (action == "renameSubject") {
            $("#renameSubject").find("input").val($("#subject-" + id).find("[data-type='name']").text());
            $("#renameSubject").data("subjectID", id);
            $("#renameSubject").modal('show');

        } else if (action == "removeSubject") {
            $("#removeSubject").data("subjectID", id);
            $("#removeSubject").modal('show');
        };
    },
    requireUpdate: function () {
        window.location.reload();
    },
    attemptRename: function (e) {
        Backstage.PreventModal = true;
        var btn = $(this);
        var i = $("#renameSubject").find("input");
        var did = $("#renameSubject").data("subjectID");
        btn.button("loading");
        $(".error").removeClass("error");
        $(".help-block").hide();
        if (i.val() == "") {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block").text("Digite o novo nome do assunto").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        }
        Backstage.postJSON("/raw/hasAreaName", { area: did, newName: i.val(), project: Metadata.idProjects }, function (data) {
            if (data.Result) {
                i.parents(".control-group").addClass("error");
                i.parent().find(".help-block").text("Já existe um assunto com o mesmo nome").show();
                btn.button("reset");
                Backstage.PreventModal = false;
                return;
            } else {
                Backstage.postJSON("/raw/renameArea", { area: did, newName: i.val() }, function (data) {
                    $("#subject-" + did).find("[data-type='name']").text(i.val());
                    Backstage.PreventModal = false;
                    $("#renameSubject").modal("hide");
                    btn.button("reset");
                }, function () {
                    i.parents(".control-group").addClass("error");
                    i.parent().find(".help-block").text("Erro durante solicitação").show();
                    btn.button("reset");
                    Backstage.PreventModal = false;
                    return;
                });
            }
        }, function () {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block").text("Erro durante solicitação").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        });
    },
    attemptRemove: function (e) {
        $("#removeSubject").modal("hide");
        $("#removingSubject").modal("show");
    }
});

$(document).ready(Backstage.Subjects.Management.boot);