﻿Namespaces("Backstage.Projects.ProjectManagement",
{
    boot: function () {
        $("a[data-action]").click(Backstage.Projects.ProjectManagement.performAction);
        $('#removingModal').on('shown', function () {
            Backstage.postJSON("/raw/removeProject", { projectID: $("#remotionWarn").find(".btn-danger").attr("data-projectid") },
            function (data) {
                if (data.Result) {
                    window.location.reload();
                } else {
                    $("#removingModal").modal('hide');
                    $("#remotionFailed").modal('show');
                }
            }, function () {
                $("#removingModal").modal('hide');
                $("#remotionFailed").modal('show');
            });
        });
        $("#changePic").find("form").submit(function (e) {
            var p = $("#changePic").find("input:last");
            if (p.val() == "") {
                p.parents(".control-group").addClass("error");
                p.parent().find(".help-block:last").text("Selecione a nova imagem").show();
                $.stopEvent(e);
                return false;
            }
        });
        $("#changePic_commit").click(function () {
            $("#changePic").find("form").trigger("submit");
        });
        $("#editModal").find("form").submit(function (e) {
            var desc = $("#editModal").find("textarea[name='desc']");
            if (desc.val() == "") {
                desc.parents(".control-group").addClass("error");
                desc.parent().find(".help-block").text("Digite a descrição do projeto").show();
                $.stopEvent(e);
                return;
            }
        });
        $("#editProj_commit").click(function () {
            $("#editModal").find("form").trigger("submit");
        });
    },
    performAction: function (e) {
        var action = $(this).attr("data-action");
        switch (action) {
            case "removeproject":
                $("#remotionWarn").find(".btn-danger").attr("data-projectid", $(this).attr("data-itemid"));
                $("#remotionWarn").modal('show');
                break;
            case "commitRemoval":
                $("#remotionWarn").modal('hide');
                $("#removingModal").modal('show');
                break;
            case "editpicture":
                $("#changePic").find("input[name='projectID']").attr("value", $(this).attr("data-itemid"));
                $("#changePic").modal('show');
                break;
            case "editdescription":
                var js = Backstage.Util.JsonSelect;
                var desc = js(Metadata, "idProjects", parseInt($(this).attr("data-itemid"), 10), true);
                $("#editModal").find("textarea").val(desc.ProjectDescription);
                $("#editModal").find("[disabled]").val(desc.ProjectName);
                $("#editModal").find("input[type='hidden']").val($(this).attr("data-itemid"));
                $("#editModal").modal("show");
            default: break;
        }
    }
});

$(document).ready(Backstage.Projects.ProjectManagement.boot);