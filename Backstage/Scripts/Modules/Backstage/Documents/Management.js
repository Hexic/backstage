﻿Namespaces("Backstage.Documents.Management",
{
    allowUpload: false,
    boot: function () {
        $("#renameDoc_commit").click(Backstage.Documents.Management.commitEditAttempt);
        $("#deleteDoc_commit").click(Backstage.Documents.Management.commitRemoveAttempt);
        $("#uploadForm").submit(Backstage.Documents.Management.uploadAttempt);
        $("#uploadDoc_commit").click(function () {
            $("#uploadForm").trigger("submit");
        });
        $("#moveDoc_commit").click(Backstage.Documents.Management.commitMoveAttempt);
    },
    commitEditAttempt: function (e) {
        Backstage.PreventModal = true;
        var btn = $(this);
        var i = $("#renameDocument").find("input");
        var did = $("#renameDocument").data("documentID");
        btn.button("loading");
        $(".error").removeClass("error");
        $(".help-block").hide();
        if (i.val() == "") {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block").text("Digite o novo nome do documento").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        }

        Backstage.postJSON("/raw/hasDocument", { area: Backstage.System.Navigation.currentSubject, document: did, newName: i.val() }, function (data) {
            if (data.Result) {
                i.parents(".control-group").addClass("error");
                i.parent().find(".help-block").text("Já existe um documento com o mesmo nome").show();
                btn.button("reset");
                Backstage.PreventModal = false;
                return;
            } else {
                Backstage.postJSON("/raw/renameDocument", { document: did, newName: i.val() }, function (data) {
                    if (data.Result) {
                        Backstage.System.Navigation.reloadSubject();
                        Backstage.PreventModal = false;
                        $("#renameDocument").modal("hide");
                    } else {
                        i.parents(".control-group").addClass("error");
                        i.parent().find(".help-block").text("Erro durante solicitação").show();
                        btn.button("reset");
                        Backstage.PreventModal = false;
                        return;
                    }
                }, function () {
                    i.parents(".control-group").addClass("error");
                    i.parent().find(".help-block").text("Erro durante solicitação").show();
                    btn.button("reset");
                    Backstage.PreventModal = false;
                    return;
                });
            }
        }, function () {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block").text("Erro durante solicitação").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        });
    },
    commitRemoveAttempt: function (e) {
        Backstage.PreventModal = true;
        var btn = $(this);
        var did = $("#deleteDocument").data("documentID");
        btn.button("loading");
        Backstage.postJSON("/raw/removeDocument", { document: did },
        function (data) {
            if (data.Result) {
                Backstage.PreventModal = false;
                $("#deleteDocument").modal("hide");
                btn.button("reset");
                Backstage.System.Navigation.reloadSubject();
                Backstage.Subjects.Loader.getDocCount();
            } else {
                Backstage.PreventModal = false;
                btn.button("reset");
                $("#deleteDocument").modal('hide');
                $("#deleteDocumentfailed").modal('show');
            }
        }, function () {
            Backstage.PreventModal = false;
            btn.button("reset");
            $("#deleteDocument").modal('hide');
            $("#deleteDocumentfailed").modal('show');
        });
    },
    uploadAttempt: function (e) {
        if (!Backstage.Documents.Management.allowUpload) $.stopEvent(e);
        else return;

        var m = $("#uploadDocument");
        var a = $("#uploadForm").find("select");
        var f = $("#uploadForm").find("input:last");
        $(".error").removeClass("error");

        if (f.val() == "") {
            f.parents(".control-group").addClass("error");
            f.parent().find(".help-block:last").text("Selecione o documento").show();
            $.stopEvent(e);
            return false;
        }

        if (a.val() == "") {
            a.parents(".control-group").addClass("error");
            a.parent().find(".help-block:last").text("Selecione o assunto").show();
            $.stopEvent(e);
            return false;
        }

        Backstage.PreventModal = true;
        var btn = $(this).find(".btn-primary");
        btn.button("loading");
        Backstage.postJSON("/raw/uniqueDoc", { name: f.val(), area: a.val() }, function (data) {
            if (data.Result) {
                Backstage.PreventModal = false;
                $("#uploadDocument").modal('hide');
                $("#uploadingDocument").modal('show');
                Backstage.PreventModal = true;
                Backstage.Documents.Management.allowUpload = true;
                $("#uploadForm").trigger("submit");
            } else {
                f.parents(".control-group").addClass("error");
                f.parent().find(".help-block:last").text("Já existe um documento com o mesmo nome armazenado no assunto especificado").show();
                $.stopEvent(e);
                Backstage.PreventModal = false;
                return false;
            }
        }, function () {
            f.parents(".control-group").addClass("error");
            f.parent().find(".help-block:last").text("Erro durante solicitação").show();
            $.stopEvent(e);
            Backstage.PreventModal = false;
            return false;
        });


    },
    commitMoveAttempt: function (e) {
        Backstage.PreventModal = true;
        var btn = $(this);
        var i = $("#moveDocument").find("select");
        var did = $("#moveDocument").data("documentID");
        btn.button("loading");
        $(".error").removeClass("error");
        $(".help-block").hide();
        if (i.val() == "") {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block:last").text("Selecione o destino").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return false;
        }
        Backstage.postJSON("/raw/uniqueDoc", { area: i.val(), document: did }, function (data) {
            if (!data.Result) {
                i.parents(".control-group").addClass("error");
                i.parent().find(".help-block:last").text("O destino já possui um arquivo com o mesmo nome").show();
                btn.button("reset");
                Backstage.PreventModal = false;
                return false;
            } else {
                Backstage.postJSON("/raw/moveDocument", { area: i.val(), document: did }, function (data) {
                    if (data.Result) {
                        Backstage.System.Navigation.reloadSubject();
                        Backstage.Subjects.Loader.getDocCount();
                        Backstage.PreventModal = false;
                        $("#moveDocument").modal("hide");
                    } else {
                        i.parents(".control-group").addClass("error");
                        i.parent().find(".help-block:last").text("Erro durante solicitação").show();
                        btn.button("reset");
                        Backstage.PreventModal = false;
                        return false;
                    }
                }, function () {
                    i.parents(".control-group").addClass("error");
                    i.parent().find(".help-block:last").text("Erro durante solicitação").show();
                    btn.button("reset");
                    Backstage.PreventModal = false;
                    return false;
                });
            }
        }, function () {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block:last").text("Erro durante solicitação").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return false;
        });
    }
});

$(document).ready(Backstage.Documents.Management.boot);