﻿Namespaces("Backstage.Configurations", {
    allowBasicSubmit: false,
    allowPasswSubmit: false,
    boot: function () {
        $("#basicForm").submit(Backstage.Configurations.basicCheck);
        $("#passwordForm").submit(Backstage.Configurations.passwordCheck);
        $("#basicCommit").click(function () { $("#basicForm").trigger("submit"); });
        $("#passwCommit").click(function () { $("#passwordForm").trigger("submit"); });
    },
    basicCheck: function (e) {
        if (Backstage.Configurations.allowBasicSubmit) return;
        else $.stopEvent(e);

        $("#errors").children().remove();
        $(".control-group").removeClass("error");
        $(".alert-block").hide();

        var f = $(this);
        var b = f.find(".btn");
        var n = f.find("#name");
        var m = f.find("#email");
        var u = f.find("#user");
        var errors = [];
        var inputs = [];

        b.button("loading");

        if (n.val() == "") errors.push("Digite seu nome");
        else {
            var ns = n.val().split(' ');
            if (ns.length < 2) { errors.push("Digite seu nome e sobrenome"); inputs.push(n); }
            else {
                if (ns[0].length < 3 || ns[1].length < 3) { errors.push("Digite seu nome e sobrenome"); inputs.push(n); }
            }
        }

        var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var userFilter = /^[a-zA-Z]+$/;
        if (!emailFilter.test(m.val())) { errors.push("Digite um endereço de e-mail válido."); inputs.push(m); }
        if (!userFilter.test(u.val())) { errors.push("Digite seu usuário."); inputs.push(u); }
        else {
            Backstage.postJSON("/raw/hasUser", { name: u.val(), current: Metadata.idUser }, function (data) {
                if (data.Result) {
                    errors.push("Já existe um usuário com o nome informado.");
                    inputs.push(u);
                }
            }, function () {
                errors.push("Não foi possível validar o usuário. Tente novamente.");
                inputs.push(u);
            });
        }

        if (errors.length > 0) {
            b.button("reset");
            _.each(errors, function (error) {
                $("#errors").append($("<li>" + error + "</li>"));
            });
            $(".alert-block").show();
            _.each(inputs, function (input) {
                input.parents(".control-group").addClass("error");
            });
        } else {
            Backstage.Configurations.allowBasicSubmit = true;
            $("#basicForm").trigger("submit");
        }




    },
    passwordCheck: function (e) {
        if (Backstage.Configurations.allowPasswSubmit) return;
        else $.stopEvent(e);

        $("#errors").children().remove();
        $(".control-group").removeClass("error");
        $(".alert-block").hide();

        var f = $(this);
        var b = f.find(".btn");
        var p1 = $("#p1");
        var p2 = $("#p2");
        var errors = [];
        var inputs = [];

        if (p1.val() == "") {
            errors.push("Preencha a nova senha.");
            inputs.push(p1);
        } else {
            if (p1.val() != p2.val()) {
                errors.push("As senhas digitadas são diferentes.");
                inputs.push(p1);
                inputs.push(p2);
            }
        }

        if (errors.length > 0) {
            _.each(errors, function (error) {
                $("#errors").append($("<li>" + error + "</li>"));
            });
            $(".alert-block").show();
            _.each(inputs, function (input) {
                input.parents(".control-group").addClass("error");
            });
        } else {
            Backstage.Configurations.allowPasswSubmit = true;
            $("#passwordForm").trigger("submit");
        }

    }
});

$(document).ready(Backstage.Configurations.boot);