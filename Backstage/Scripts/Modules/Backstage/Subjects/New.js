﻿Namespaces("Backstage.Subjects.New",
{
    boot: function () {
        $("#newSubject_commit").click(Backstage.Subjects.New.commitAttempt);
    },
    commitAttempt: function (e) {
        Backstage.PreventModal = true;
        var btn = $(this);
        var i = $("#newSubject").find("input");
        btn.button("loading");
        $(".error").removeClass("error");
        $(".help-block").hide();
        if (i.val() == "") {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block").text("Digite o nome do assunto").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        }
        Backstage.postJSON("/raw/hasArea", { area: i.val(), project: Metadata.idProjects }, function (data) {
            if (data.Result) {
                i.parents(".control-group").addClass("error");
                i.parent().find(".help-block").text("Já existe um assunto com o mesmo nome").show();
                btn.button("reset");
                Backstage.PreventModal = false;
                return;
            } else {
                Backstage.postJSON("/raw/createArea", { area: i.val(), project: Metadata.idProjects }, function (data) {
                    if (data.Result) {
                        i.val("");
                        Backstage.PreventModal = false;
                        $("#newSubject").modal('hide');
                        if (Namespaces.exists("Backstage.Subjects.Loader")) Backstage.Subjects.Loader.load();
                        if (Namespaces.exists("Backstage.System.Navigation")) Backstage.System.Navigation.updateCurrentSubject();
                        if (Namespaces.exists("Backstage.Subjects.Management")) Backstage.Subjects.Management.requireUpdate();
                        btn.button("reset");
                    } else {
                        i.parents(".control-group").addClass("error");
                        i.parent().find(".help-block").text("Erro durante solicitação").show();
                        btn.button("reset");
                        Backstage.PreventModal = false;
                    }
                }, function () {
                    i.parents(".control-group").addClass("error");
                    i.parent().find(".help-block").text("Erro durante solicitação").show();
                    btn.button("reset");
                    Backstage.PreventModal = false;
                });
            }
        }, function () {
            i.parents(".control-group").addClass("error");
            i.parent().find(".help-block").text("Erro durante solicitação").show();
            btn.button("reset");
            Backstage.PreventModal = false;
        });
    }

});

$(document).ready(Backstage.Subjects.New.boot);