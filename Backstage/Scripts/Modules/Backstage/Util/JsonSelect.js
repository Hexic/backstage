﻿/*
Script: JsonSelect.js (Singularity.Util.JsonSelect)
Pesquisa dados entre uma array Json por uma propriedade [p] de valor [v]
@author Victor Gama (@victrgama)
@version 1.1.0
*/
Namespaces("Backstage.Util.JsonSelect");
function selectJson() {
    var results = [];
    var that = this;
    function process(o, p, v, s) {
        s = s || false;
        results = [];
        _.each(o, function (ob) {
            if (typeof (ob.sort) === "undefined") {
                if (ob[p] === v) { results.push(ob); }
            } else {
                process(ob, p, v)
            }
        });
        if (!s) { return results; }
        else {
            if (results.length == 1) { return results[0]; }
            else { return results; }
        }
    };
    return process;
}
Backstage.Util.JsonSelect = selectJson();