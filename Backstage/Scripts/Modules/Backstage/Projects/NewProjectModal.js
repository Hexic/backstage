﻿Namespaces("Backstage.Projects.NewProjectModal",
{
    allowSubmit: false,
    boot: function () {
        $("#newProj").find(".form-horizontal").submit(Backstage.Projects.NewProjectModal.submitAttempt);
        $("#newProj_commit").click(Backstage.Projects.NewProjectModal.commit);
    },
    commit: function (e) {
        $(".error").removeClass("error");
        $("[class='.help-block']").hide();
        var proj = $("#newProj").find("input[name='name']");
        var desc = $("#newProj").find("textarea[name='desc']");
        var file = $("#newProj").find("input[name='file']");
        var btn = $(this);
        btn.button("loading");
        Backstage.PreventModal = true;

        if (proj.val() == "") {
            proj.parents(".control-group").addClass("error");
            proj.parent().find(".help-block").text("Digite o nome do projeto").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        }

        if (desc.val() == "") {
            desc.parents(".control-group").addClass("error");
            desc.parent().find(".help-block").text("Digite a descrição do projeto").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        }

        if (file.val() == "") {
            file.parents(".control-group").addClass("error");
            file.parent().find(".help-block:last").text("Selecione a imagem do projeto").show();
            btn.button("reset");
            Backstage.PreventModal = false;
            return;
        }


        Backstage.postJSON("/raw/hasProject", { name: proj.val() }, function (data) {
            if (data.Result) {
                proj.parents(".control-group").addClass("error");
                proj.parent().find(".help-block").text("Já existe um projeto com o mesmo nome.").show();
                Backstage.PreventModal = false;
                btn.button("reset");
            } else {
                Backstage.Projects.NewProjectModal.allowSubmit = true;
                $("#newProj").find(".form-horizontal").trigger("submit");
            }
        }, function () {
            proj.parents(".control-group").addClass("error");
            proj.parent().find(".help-block").text("Falha na solicitação.").show();
            btn.button("reset");
            Backstage.PreventModal = false;
        });
    },
    submitAttempt: function (e) {
        if (!Backstage.Projects.NewProjectModal.allowSubmit) { $.stopEvent(e); $("#newProj_commit").trigger("click"); return false; }

    }
});

$(document).ready(Backstage.Projects.NewProjectModal.boot);