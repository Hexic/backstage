﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackstageData;
using BackstageData.Models;

namespace Backstage.Controllers
{
    [Authorize]
    public class SubjectController : BaseController
    {
        //
        // GET: /Subject/

        public ActionResult Index(string id)
        {
            if (id == null) return RedirectToAction("Index", "Home");
            var M = Store.SelectMultiple<ProjectModel>("SafeProjectName = @n", new MySql.Data.MySqlClient.MySqlParameter[] 
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@n", id)
                });
            if (M.Length != 1) return View("NotFound");
            else return View(M[0]);
        }

    }
}
