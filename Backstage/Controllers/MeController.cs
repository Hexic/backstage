﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackstageData;
using BackstageData.Models;

namespace Backstage.Controllers
{
    [Authorize]
    public class MeController : BaseController
    {
        //
        // GET: /Me/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Configurations()
        {
            return View(Store.SelectSingle<UserModel>((uint)Models.UserAuthenticationModel.UserID));
        }
        [HttpPost]
        public ActionResult UpdateUserBasic(string name, string email, string user)
        {
            var u = Store.SelectSingle<UserModel>((uint)Models.UserAuthenticationModel.UserID);
            u.Name = name;
            u.Email = email;
            u.Username = user;
            if (Store.Insert(u) > 0) TempData["BasicOk"] = true;
            else TempData["BasicFailed"] = true;

            return RedirectToAction("Configurations");
        }
        [HttpPost]
        public ActionResult ChangePass(string newPass)
        {
            var u = Store.SelectSingle<UserModel>((uint)Models.UserAuthenticationModel.UserID);
            u.Password = newPass;
            if (Store.Insert(u) > 0) TempData["PasswordOk"] = true;
            else TempData["PasswordFailed"] = true;

            return RedirectToAction("Configurations");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}
