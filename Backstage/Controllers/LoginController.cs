﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackstageData;
using BackstageData.Models;

namespace Backstage.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        [HttpGet]
        public ActionResult Index()
        {
            if (Models.UserAuthenticationModel.IsAuthenticated) return RedirectToAction("Index", "Home");
            return View();
        }

        //
        // Post: /Login/
        [HttpPost]
        public ActionResult Index(string username, string password, string redir)
        {
            if (username == "" || password == "")
            {
                TempData["failed"] = true;
                return View();
            }
            var data = Store.SelectMultiple<UserModel>("username = @u and password = PASSWORD(@p)", new MySql.Data.MySqlClient.MySqlParameter[] {
                new MySql.Data.MySqlClient.MySqlParameter("@u",username),
                new MySql.Data.MySqlClient.MySqlParameter("@p", password)
            });
            if (data.Length != 1)
            {
                TempData["failed"] = true;
                return View();
            }
            else
            {
                var u = data[0];
                Models.UserAuthenticationModel.Role = u.Role;
                Models.UserAuthenticationModel.UserID = u.idUser;
                Models.UserAuthenticationModel.Username = u.Username;
                Models.UserAuthenticationModel.RealName = u.Name;
                if (redir == "")
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return Redirect(redir);
                }
            }
        }

    }
}
