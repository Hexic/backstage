﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackstageData;
using BackstageData.Models;
using System.IO;

namespace Backstage.Controllers
{
    [Authorize]
    public class ProjectController : BaseController
    {
        //
        // GET: /Project/{id}

        public ActionResult Index(string id)
        {
            if (id == null) return RedirectToAction("Index", "Home");
            var M = Store.SelectMultiple<ProjectModel>("SafeProjectName = @n", new MySql.Data.MySqlClient.MySqlParameter[] 
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@n", id)
                });
            if (M.Length != 1) return View("NotFound");
            else return View(M[0]);
        }

        public ActionResult HomePage(string id)
        {
            var M = Store.SelectMultiple<ProjectModel>("SafeProjectName = @n", new MySql.Data.MySqlClient.MySqlParameter[] 
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@n", id)
                });
            if (M.Length != 1) return View("NotFound");
            else return View(M[0]);
        }

        public ActionResult GetDocument(string id, string doc)
        {
            var M = Store.SelectMultiple<ProjectModel>("SafeProjectName = @n", new MySql.Data.MySqlClient.MySqlParameter[] 
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@n", id)
                });
            if (M.Length != 1) return View("NotFound");
            else {
                var D = Store.SelectMultiple<DocModel>("DocInternalID = @d", new MySql.Data.MySqlClient.MySqlParameter[] 
                {
                    new MySql.Data.MySqlClient.MySqlParameter("@d", doc)
                });
                if (D.Length != 1) return View("NotFound");
                else
                {
                    var d = D[0];
                    SeenDocModel s = new SeenDocModel()
                    {
                        DocID = d.idDocs,
                        UserID = (uint)Models.UserAuthenticationModel.UserID
                    };
                    Store.Insert(s);
                    var p = Path.Combine(Server.MapPath("~/App_data"), d.DocInternalID);
                    FileInfo x = new FileInfo(p);
                    Response.Clear();
                    string fn = "";
                    foreach (var c in d.DocName)
                    {
                        if (!Path.GetInvalidFileNameChars().Contains(c)) fn += c.ToString();
                    }
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + d.DocName);
                    Response.ContentType = "application/octet-stream";
                    Response.TransmitFile(p);
                    Response.End();
                    return null;
                       
                }
            }
        }

        // controller = "Project", action = "GetSubject", id = UrlParameter.Optional, sub = UrlParameter.Optional
        public ActionResult GetSubject(string id, string sub)
        {
            id = "dummy";
            uint output;
            bool valid = uint.TryParse(sub, out output);
            if (!valid) return View("NotFound");
            
            var M = Store.SelectSingle<AreaModel>(output);
            if (M == null) return View("NotFound");
            else return View(M);
        }

    }
}
