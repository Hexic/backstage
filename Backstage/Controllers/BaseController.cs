﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Principal;

namespace Backstage.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Models.UserAuthenticationModel.IsAuthenticated)
            {
                filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(Models.UserAuthenticationModel.Username), new string[] { Models.UserAuthenticationModel.Role });
            }
            base.OnAuthorization(filterContext);
        }
    }
}
