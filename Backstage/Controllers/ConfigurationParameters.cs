﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Backstage.Controllers
{
    public static class ConfigurationParameters
    {
        public static bool isConfigurationValid()
        {
            var valid = true;
            var errors = new List<string>();
            try
            {
                var d = ConfigurationManager.AppSettings["BackstageConnectionString"];
                if (string.IsNullOrWhiteSpace(d))
                {
                    valid = false;
                    errors.Add("A variável <code>BackstageConnectionString</code> não foi definida.");
                }
                d = ConfigurationManager.AppSettings["BackstageDeliverEmailNotifications"];
                bool deliverActivated;
                bool isvaliddeliver = bool.TryParse(d, out deliverActivated);
                if(!isvaliddeliver) {
                    valid = false;
                    errors.Add("A variável <code>BackstageDeliverEmailNotifications</code> possui um valor inválido.");
                }
                if (deliverActivated)
                {
                    d = ConfigurationManager.AppSettings["BackstageSMTPUsername"];
                    if (string.IsNullOrWhiteSpace(d))
                    {
                        valid = false;
                        errors.Add("A variável <code>BackstageSMTPUsername</code> não foi definida.");
                    }
                    d = ConfigurationManager.AppSettings["BackstageSMTPPassword"];
                    if (String.IsNullOrWhiteSpace(d))
                    {
                        valid = false;
                        errors.Add("A variável <code>BackstageSMTPPassword</code> não foi definida.");
                    }
                    d = ConfigurationManager.AppSettings["BackstageSMTPServer"];
                    if (string.IsNullOrWhiteSpace(d))
                    {
                        valid = false;
                        errors.Add("A variável <code>BackstageSMTPServer</code> não foi definida.");
                    }
                    d = ConfigurationManager.AppSettings["BackstageSMTPPort"];
                    int dummyint;
                    bool isvalidport = int.TryParse(d, out dummyint);
                    if (!isvalidport)
                    {
                        valid = false;
                        errors.Add("O valor da variável <code>BackstageSMTPPort</code> é inválido.");
                    }
                }
            }
            catch (Exception)
            {
                valid = false;
                errors.Add("Não foi possível localizar as configurações do sistema.");
            }
            if (!valid) HttpContext.Current.Application["FailedConfigChecks"] = errors.ToArray();
            return valid;
        }
    }
}