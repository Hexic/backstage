﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Fil = System.IO.File;

namespace Backstage.Controllers
{
    
    [Authorize]
    public class ChangelogController : BaseController
    {
        //
        // GET: /Changelog/
        
        public ActionResult Index()
        {
            var f = Fil.ReadAllText(Server.MapPath("~/Data/Changelog/ChangelogDatabase.xml"));
            var db = XDocument.Parse(f, LoadOptions.None);
            var els = db.Element("BackstageChangelog").Elements("Changelog");
            var ret = new List<Models.ChangeLogEntry>();
            foreach (var el in els)
            {
                var e = new Models.ChangeLogEntry()
                {
                    Version = el.Attribute("Version").Value,
                    Changes = new List<Models.ChangeEntry>()
                };
                var changes = el.Elements("Change");
                foreach (var c in changes)
                {
                    var ch = (new Models.ChangeEntry()
                    {
                        Description = c.Value
                    });
                    if (c.HasAttributes) ch.Icon = "icon-" + c.Attribute("Icon").Value;
                    else ch.Icon = "icon-chevron-right";
                    e.Changes.Add(ch);
                }
                ret.Add(e);
            }
            return View(ret);
        }

    }
}
