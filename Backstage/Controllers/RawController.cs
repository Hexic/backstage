﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BackstageData;
using BackstageData.Models;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Backstage.Controllers
{
    [Authorize]
    public class RawController : BaseController
    {
        //
        // GET: /Raw/
        [HttpPost]
        public ActionResult hasProject(string name)
        {
            var d = Store.SelectMultiple<ProjectModel>();

            return Json(new
            {
                Result = d.Any(p => p.ProjectName.Equals(name, StringComparison.OrdinalIgnoreCase))
            });
        }
        [HttpPost]
        public ActionResult NewProject(string name, string desc, HttpPostedFileBase file)
        {
            ProjectModel M = new ProjectModel()
            {
                ProjectDescription = desc,
                ProjectName = name,
                SafeName = name
            };
            if (Store.Insert(M) > 0)
            {
                var p = Path.Combine(Server.MapPath("~/Data/Images"), M.SafeName + ".png");
                file.SaveAs(p);
            }
            else
            {
                TempData["newfailed"] = true;
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public ActionResult removeProject(string projectID)
        {
            uint output;
            bool valid = uint.TryParse(projectID, out output);
            if (!valid) return Json(new { Result = false });
            // List'em all.
            var p = Store.SelectSingle<ProjectModel>(output);
            var areas = Store.SelectMultiple<AreaModel>("Project = " + output).ToList();
            var docs = new List<DocModel>();
            var seen = new List<SeenDocModel>();
            foreach (var a in areas) docs.AddRange(Store.SelectMultiple<DocModel>("Area = " + a.idAreas));
            foreach (var d in docs) seen.AddRange(Store.SelectMultiple<SeenDocModel>("where DocID = " + d.idDocs));


            // Remove Seen Documents.
            foreach (var s in seen) Store.Destroy(s);
            // Remove Documents.
            foreach (var d in docs)
            {
                try
                {
                    System.IO.File.Delete(Path.Combine(Server.MapPath("~/App_data"), d.DocInternalID));
                }
                catch (Exception)
                { }
                Store.Destroy(d);
            }
            // Remove Areas.
            foreach (var a in areas) Store.Destroy(a);
            // Remove Project Image
            try
            {
                System.IO.File.Delete(Path.Combine(Server.MapPath("~/Data/Images"), p.SafeName + ".png"));
            }
            catch (Exception)
            { }
            // Remove Project
            if(p != null) Store.Destroy(p);

            return Json(new { Result = true });
        }
        [HttpPost]
        public ActionResult ChangeProjectImage(string projectID, HttpPostedFileBase file)
        {
            uint output;
            bool valid = uint.TryParse(projectID, out output);
            if (!valid) return RedirectToAction("Index", "Home");
            var M = Store.SelectSingle<ProjectModel>(output);
            var p = Path.Combine(Server.MapPath("~/Data/Images"), M.SafeName + ".png");
            System.IO.File.Delete(p);
            file.SaveAs(p);
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public ActionResult EditProject(string projectID, string desc)
        {
            uint output;
            bool valid = uint.TryParse(projectID, out output);
            if (!valid) return RedirectToAction("Index", "Home");
            var M = Store.SelectSingle<ProjectModel>(output);
            M.ProjectDescription = desc;
            if (Store.Insert(M) < 1)
            {
                TempData["editfailed"] = true;
            }
            return RedirectToAction("Index", "Home");

            
        }
        [HttpPost]
        public ActionResult getSubjects(string id)
        {
            uint output;
            bool valid = uint.TryParse(id, out output);
            if (!valid) return Json(new { length = 0 });
            var l = Store.SelectMultiple<AreaModel>("Project = " + output);
            /* List<T>.Sort() não é suficiente para organizar uma list de AreaModel (tal como provavelmente nenhum outro
             * modelo do EF. Por esse motivo, substituiremos List<T>.Sort() por uma expressão em LINQ, porque ele é lindo
             * e faz tudo funcionar <3
             * (Além do mais, repare que não temos mais nada envolvendo List<T>, agora usamos só a array fornecida pelo
             * DynamicData, transformamos-a em System.Linq.IOrderedEnumerable<AreaModel>, e depois para Array de novo.
             * 
             * Just works.
             * 
             * -- Removida redundancia em .ToArray() que era solicitado novamente enquanto o resultado em JSON era
             * gerado.
             */
            if (l.Length > 0) l = (from a in l
                                   orderby a.AreaName ascending
                                   select a).ToArray();
            return Json(new
            {
                length = l.Length,
                subjects = l
            });
        }
        [HttpPost]
        public ActionResult getDocCount(string area)
        {
            uint output;
            bool valid = uint.TryParse(area, out output);
            if (!valid) return Json(new { DocsCount = 0 });
            var l = Store.SelectMultiple<DocModel>("Area = " + output);
            return Json(new
            {
                DocsCount = l.Length,
            });
        }
        [HttpPost]
        public ActionResult hasArea(string area, string project)
        {
            uint output;
            bool valid = uint.TryParse(project, out output);
            if (!valid) return Json(new { Result = true });
            var ars = Store.SelectMultiple<AreaModel>("Project = " + output);
            return Json(new { Result = ars.Any(ar => ar.AreaName.Equals(area, StringComparison.OrdinalIgnoreCase)) });
        }
        [HttpPost]
        public ActionResult createArea(string area, string project)
        {
            uint output;
            bool valid = uint.TryParse(project, out output);
            if (!valid) return Json(new { Result = true });
            AreaModel M = new AreaModel()
            {
                AreaName = area,
                Project = output
            };
            return Json(new { Result = Store.Insert(M) > 0 });
        }
        [HttpPost]
        public ActionResult hasDocument(string area, string document, string newName)
        {
            uint areaOutput, documentOutput;
            bool valid = uint.TryParse(area, out areaOutput);
            if (!valid) return Json(new { Result = true });
            valid = uint.TryParse(document, out documentOutput);
            if (!valid) return Json(new { Result = true });
            var areaDocs = Store.SelectMultiple<DocModel>("Area = " + areaOutput).ToList();
            var doc = Store.SelectSingle<DocModel>(documentOutput);
            if (doc == null) return Json(new { Result = true });
            areaDocs.Remove(areaDocs.FirstOrDefault(d => d.DocInternalID == doc.DocInternalID));
            var extension = Path.GetExtension(doc.DocName);
            return Json(new { Result = areaDocs.Any(d => d.DocName.Equals(newName + extension, StringComparison.OrdinalIgnoreCase)) });
        }
        [HttpPost]
        public ActionResult removeDocument(string document)
        {
            uint output;
            bool valid = uint.TryParse(document, out output);
            if (!valid) return Json(new { Result = false });
            var doc = Store.SelectSingle<DocModel>(output);
            if (doc == null) return Json(new { Result = false });
            var seenEntries = Store.SelectMultiple<SeenDocModel>("DocID = " + output);
            foreach(var e in seenEntries) Store.Destroy(e);
            System.IO.File.Delete(Path.Combine(Server.MapPath("~/App_data"), doc.DocName));
            Store.Destroy(doc);
            return Json(new { Result = true });
        }
        [HttpPost]
        public ActionResult UploadDocument(string safename, string areaid, HttpPostedFileBase file)
        {
            uint output;
            bool valid = uint.TryParse(areaid, out output);
            if (!valid)
            {
                TempData["failedupload"] = true;
                return Redirect("~/Project/" + safename);
            }
            string uid = Guid.NewGuid().ToString();

            AreaModel a = Store.SelectSingle<AreaModel>(output);

            DocModel d = new DocModel()
            {
                Area = output,
                DocName = file.FileName,
                DocInternalID = uid,
                Sender = (uint)Models.UserAuthenticationModel.UserID
            };
            bool deliveryMail = bool.Parse(ConfigurationManager.AppSettings["BackstageDeliverEmailNotifications"]);
             
            try
            {
                if (deliveryMail)
                {

                    string smtpUsername = ConfigurationManager.AppSettings["BackstageSMTPUsername"],
                           smtpPassword = ConfigurationManager.AppSettings["BackstageSMTPPassword"],
                           smtpServer = ConfigurationManager.AppSettings["BackstageSMTPServer"];
                    int smtpPort = int.Parse(ConfigurationManager.AppSettings["BackstageSMTPPort"]);
                           


                    string TitleMatch = "{Title=\".+\"}";
                    string TitleExtractor = "^{Title=\"(.+)\"}$";
                    string SenderMatch = "{Sender=\".+\"}";
                    string SenderExtractor = "^{Sender=\"(.+)\"}$";
                    string SenderMailMatch = "{SenderMail=\".+\"}";
                    string SenderMailExtractor = "^{SenderMail=\"(.+)\"}$";

                    var docModel = System.IO.File.ReadAllLines(Server.MapPath("~/Data/Models/Mail/NewDocNotification.model"));
                    var title = "";
                    var sender = "";
                    var sendermail = "";
                    foreach (string line in docModel)
                    {
                        var reg = new Regex(TitleMatch);
                        if (reg.IsMatch(line))
                        {
                            reg = new Regex(TitleExtractor);
                            title = reg.Match(line).Groups[1].Value;
                            break;
                        }
                    }
                    foreach (string line in docModel)
                    {
                        var reg = new Regex(SenderMatch);
                        if (reg.IsMatch(line))
                        {
                            reg = new Regex(SenderExtractor);
                            sender = reg.Match(line).Groups[1].Value;
                            break;
                        }
                    }
                    foreach (string line in docModel)
                    {
                        var reg = new Regex(SenderMailMatch);
                        if (reg.IsMatch(line))
                        {
                            reg = new Regex(SenderMailExtractor);
                            sendermail = reg.Match(line).Groups[1].Value;
                            break;
                        }
                    }

                    var messagebody = string.Join("\r\n", docModel);
                    var replacer = new Regex(TitleMatch); // Inicia com o TitleMatch para podermos remove-lo da mensagem
                    // {Username} -- Dynamic
                    // {SenderName} -- Fix
                    // {DocumentName} -- Fix
                    // {ProjectName} -- Fix
                    // {ProjectUrl} -- Fix

                    // Remove title, sender, sendermail
                    messagebody = replacer.Replace(messagebody, "");
                    replacer = new Regex(SenderMatch);
                    messagebody = replacer.Replace(messagebody, "");
                    replacer = new Regex(SenderMailMatch);
                    messagebody = replacer.Replace(messagebody, "");
                    // ~le sendername~
                    replacer = new Regex("{SenderName}");
                    messagebody = replacer.Replace(messagebody, Models.UserAuthenticationModel.RealName.Split(' ')[0]);
                    // ~le documentname~
                    replacer = new Regex("{DocumentName}");
                    messagebody = replacer.Replace(messagebody, Path.GetFileNameWithoutExtension(d.DocName));
                    // ~le projectname~
                    replacer = new Regex("{ProjectName}");
                    messagebody = replacer.Replace(messagebody, a.ProjectObject.ProjectName);
                    // ~le projecturl~
                    replacer = new Regex("{ProjectUrl}");
                    messagebody = replacer.Replace(messagebody, "http://" + Request.ServerVariables["HTTP_HOST"] + "/Project/" + a.ProjectObject.SafeName);

                    replacer = new Regex("{Username}");
                    foreach (var u in Store.SelectMultiple<UserModel>())
                    {
                        if (u.idUser == Models.UserAuthenticationModel.UserID) continue;
                        try
                        {

                            string body = replacer.Replace(messagebody, u.Name.Split(' ')[0]);
                            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                            message.To.Add(u.Email);
                            message.Subject = title;
                            message.From = new System.Net.Mail.MailAddress(sendermail, sender);
                            message.Body = body;
                            message.IsBodyHtml = true;
                            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
                            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(smtpServer, smtpPort);
                            smtp.UseDefaultCredentials = false;
                            smtp.Credentials = SMTPUserInfo;
                            smtp.Send(message);

                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                var p = Path.Combine(Server.MapPath("~/app_data"), uid);
                file.SaveAs(p);
            }
            catch (Exception)
            {
                TempData["failedupload"] = true;
                return Redirect("~/Project/" + safename);
            }

            uint ins = Store.Insert(d);
            if (ins < 1)
            {
                TempData["failedupload"] = true;
                return Redirect("~/Project/" + safename);
            }


            SeenDocModel sm = new SeenDocModel()
            {
                DocID = ins,
                UserID = d.Sender
            };
            Store.Insert(sm);

            TempData["uploadok"] = true;
            return Redirect("~/Project/" + safename);

        }
        [HttpPost]
        public ActionResult renameDocument(string document, string newName)
        {
            uint output;
            bool valid = uint.TryParse(document, out output);
            if (!valid) return Json(new { Result = false });
            var doc = Store.SelectSingle<DocModel>(output);
            if (doc == null) return Json(new { Result = false });
            var ext = Path.GetExtension(doc.DocName);
            doc.DocName = newName + ext;
            return Json(new { Result = Store.Insert(doc) > 0 });
        }
        [HttpPost]
        public ActionResult hasAreaName(string area, string newName, string project)
        {
            uint areaOutput, projectOutput;
            bool valid = uint.TryParse(area, out areaOutput);
            if (!valid) return Json(new { Result = true });
            valid = uint.TryParse(project, out projectOutput);
            if (!valid) return Json(new { Result = true });
            var areas = Store.SelectMultiple<AreaModel>("Project = " + projectOutput).ToList();
            areas.Remove(areas.FirstOrDefault(a => a.idAreas == areaOutput));
            return Json(new { Result = areas.Any(a => a.AreaName.Equals(newName, StringComparison.OrdinalIgnoreCase)) });
        }
        [HttpPost]
        public ActionResult renameArea(string area, string newName)
        {
            uint areaOutput;
            bool valid = uint.TryParse(area, out areaOutput);
            if (!valid) return Json(new { Result = false });
            var m = Store.SelectSingle<AreaModel>(areaOutput);
            m.AreaName = newName;
            return Json(new { Result = Store.Insert(m) > 0 });
        }
        [HttpPost]
        public ActionResult removeArea(string areaID)
        {
            uint output;
            bool valid = uint.TryParse(areaID, out output);
            if (!valid) return Json(new { Result = false });
            var area = Store.SelectSingle<AreaModel>(output);
            if(area == null) return Json(new { Result = false });

            var docs = new List<DocModel>();
            var seen = new List<SeenDocModel>();
            docs.AddRange(Store.SelectMultiple<DocModel>("Area = " + output));
            foreach (var d in docs) seen.AddRange(Store.SelectMultiple<SeenDocModel>("where DocID = " + d.idDocs));
            // Remove Seen Documents.
            foreach (var s in seen) Store.Destroy(s);
            // Remove Documents.
            foreach (var d in docs)
            {
                try
                {
                    System.IO.File.Delete(Path.Combine(Server.MapPath("~/App_data"), d.DocInternalID));
                }
                catch (Exception)
                { }
                Store.Destroy(d);
            }
            Store.Destroy(area);

            return Json(new { Result = true });

        }
        [HttpPost]
        public ActionResult uniqueDoc(string name, string area)
        {
            uint areaOutput;
            bool valid = uint.TryParse(area, out areaOutput);
            if (!valid) return Json(new { Result = false });
            name = Path.GetFileName(name);
            var docs = Store.SelectMultiple<DocModel>("Area = " + areaOutput);
            return Json(new { Result = !(docs.Any(d => d.DocName.Equals(name, StringComparison.OrdinalIgnoreCase))) });
        }
        [HttpPost]
        public ActionResult moveDocument(string area, string document)
        {
            uint areaOutput, documentOutput;
            bool valid = uint.TryParse(area, out areaOutput);
            if (!valid) return Json(new { Result = false });
            valid = uint.TryParse(document, out documentOutput);
            if (!valid) return Json(new { Result = false });
            var model = Store.SelectSingle<DocModel>(documentOutput);
            model.Area = areaOutput;
            return Json(new { Result = Store.Insert(model) > 0 });
        }
        [HttpPost]
        public ActionResult hasUser(string name, string current)
        {
            uint output;
            bool valid = uint.TryParse(current, out output);
            if (!valid) return Json(new { Result = true });

            var users = Store.SelectMultiple<UserModel>().ToList();

            users.Remove(users.FirstOrDefault(u => u.idUser == output));

            return Json(new { Result = users.Any(u => u.Username.Equals(name, StringComparison.OrdinalIgnoreCase)) });

        }
    }
}
