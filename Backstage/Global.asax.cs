﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using MySql.Data.MySqlClient;

namespace Backstage
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "ProjectsFriendlyURL",
                "Project/{id}",
                new { controller = "Project", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                "ProjectsSubjectManagement",
                "Project/{id}/Subjects",
                new { controller = "Subject", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                "ProjectDocumentDownloadPage",
                "Project/{id}/Document/{doc}",
                new { controller = "Project", action = "GetDocument", id = UrlParameter.Optional, doc = UrlParameter.Optional });
            routes.MapRoute(
                "ProjectSubjectPage",
                "Project/{id}/Subject/{sub}",
                new { controller = "Project", action = "GetSubject", id = UrlParameter.Optional, sub = UrlParameter.Optional });
            routes.MapRoute(
                "ProjectsInvertedUrl",
                "Project/{id}/{action}",
                new { controller = "Project", action = "Index", id = UrlParameter.Optional });
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            var cValid = Controllers.ConfigurationParameters.isConfigurationValid();
            Application["Healthy"] = cValid;
            if (cValid) BackstageData.Settings.ConnectionString = ConfigurationManager.AppSettings["BackstageConnectionString"];
            else
            {
                
                var doc = File.ReadAllText(Server.MapPath("~/Data/Models/InvalidConfiguration.model"));
                var regex = new Regex("{ErrorList}");
                var errorList = new StringBuilder();
                foreach (string error in (string[])Application["FailedConfigChecks"])
                {
                    errorList.Append(string.Format("<li><i class=\"icon-remove\"></i> {0}</li>", error));
                }
                doc = regex.Replace(doc, errorList.ToString());
                Application["InvalidConfigDoc"] = doc;
                return;
            }
            try
            {
                MySqlConnection k = new MySqlConnection(BackstageData.Settings.ConnectionString);
                k.Open();
                k.Ping();
                k.Close();
            }
            catch (Exception ex)
            {
                Application["Healthy"] = false;
                var doc = File.ReadAllText(Server.MapPath("~/Data/Models/InvalidConfiguration.model"));
                var regex = new Regex("{ErrorList}");
                var errorList = new StringBuilder();
                errorList.Append(string.Format("<li><i class=\"icon-remove\"></i> {0}</li>", "Não foi possível conectar-se ao servidor MySQL especificado.<br/>Erro: " + ex.Message));
                doc = regex.Replace(doc, errorList.ToString());
                Application["InvalidConfigDoc"] = doc;
                return;
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!(bool)Application["Healthy"])
            {
                var validrequesttermination = "css,png,ico".Split(',');
                var valid = false;
                foreach (var termination in validrequesttermination) if (Request.ServerVariables["Url"].EndsWith(termination)) valid = true;
                if (!valid)
                {
                    Response.Clear();
                    Response.Write((string)Application["InvalidConfigDoc"]);
                    Response.End();
                }
            }
        }
        

        protected void Session_Start(object sender, EventArgs e)
        {
            // HACKHACK: Não estava querendo funcionar, aí eu arrumei.
            string sessionId = Session.SessionID;
        }
    }
}