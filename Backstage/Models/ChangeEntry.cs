﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backstage.Models
{
    public class ChangeEntry
    {
        public string Description { get; set; }
        public string Icon { get; set; }
    }
}