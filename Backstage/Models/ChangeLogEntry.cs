﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backstage.Models
{
    public class ChangeLogEntry
    {
        public string Version { get; set; }
        public List<ChangeEntry> Changes { get; set; }
    }
}