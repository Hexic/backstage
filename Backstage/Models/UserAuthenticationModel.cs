﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backstage.Models
{
    using context = HttpContext;
    public static class UserAuthenticationModel
    {
        public static string Username
        {
            get { return context.Current.Session["Username"].ToString() ?? ""; }
            set { context.Current.Session["Username"] = value; }
        }
        public static uint? UserID
        {
            get { return (context.Current.Session["UserID"] == null ? null : context.Current.Session["UserID"] as uint?); }
            set { context.Current.Session["UserID"] = value; }
        }
        public static string Role
        {
            get { return context.Current.Session["Role"].ToString() ?? ""; }
            set { context.Current.Session["Role"] = value; }
        }

        public static bool IsAuthenticated
        {
            get
            {
                return (context.Current.Session["UserID"] != null);
            }
        }

        public static string RealName
        {
            get { return context.Current.Session["RealName"].ToString() ?? ""; }
            set { context.Current.Session["RealName"] = value; }
        }

        public static string PrettyName
        {
            get
            {
                var raw = RealName.Split(' ');
                return String.Format("{0} {1}.", raw[0], raw[1][0]);

            }
        }
    }
}