﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData.Models
{
    [Table("Docs")]
    public class DocModel
    {
        [KeyColumn]
        public uint idDocs { get; set; }
        [Column]
        public string DocInternalID { get; set; }
        [Column]
        public uint Area { get; set; }
        [Column]
        public string DocName { get; set; }
        [Column]
        public uint Sender { get; set; }

        public UserModel SenderObject
        {
            get
            {
                return Store.SelectSingle<UserModel>(Sender);
            }
        }

        public AreaModel AreaObject
        {
            get
            {
                return Store.SelectSingle<AreaModel>(Area);
            }
        }
        
        public DocModel()
        {

        }
    }
}
