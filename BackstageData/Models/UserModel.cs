﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData.Models
{
    [Table("Users")]
    public class UserModel
    {
        [KeyColumn]
        public uint idUser { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string Username { get; set; }
        [WriteOnlyColumn(ExtendFunction=EngineFunction.Password)]
        public string Password { internal get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string Role { get; set; }

        public UserModel()
        { }
    }
}
