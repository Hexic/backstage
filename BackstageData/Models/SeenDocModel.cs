﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData.Models
{
    [Table("SeenDocs")]
    public class SeenDocModel
    {
        [KeyColumn]
        public uint idSeenDoc { get; set; }
        [Column]
        public uint DocID { get; set; }
        [Column]
        public uint UserID { get; set; }

        public DocModel DocObject
        {
            get
            {
                return Store.SelectSingle<DocModel>(DocID);
            }
        }
        public UserModel UserObject
        {
            get
            {
                return Store.SelectSingle<UserModel>(UserID);
            }
        }

        public SeenDocModel()
        {

        }

    }
}
