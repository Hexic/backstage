﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData.Models
{
    [Table("Areas")]
    public class AreaModel
    {
        [KeyColumn]
        public uint idAreas { get; set; }
        [Column]
        public string AreaName { get; set; }
        [Column]
        public uint Project { get; set; }

        public ProjectModel ProjectObject
        {
            get
            {
                return Store.SelectSingle<ProjectModel>(Project);
            }
        }

        public AreaModel()
        {

        }
    }
}
