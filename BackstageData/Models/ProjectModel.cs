﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData.Models
{
    [Table("Projects")]
    public class ProjectModel
    {
        [KeyColumn]
        public uint idProjects { get; set; }
        [Column]
        public string ProjectName { get; set; }
        [Column]
        public string SafeProjectName { get; set; }
        [Column]
        public string ProjectDescription { get; set; }

        public string SafeName
        {
            get { return SafeProjectName; }
            set
            {
                var safe = "0123456789ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwyxz".ToCharArray();
                var res = "";
                foreach (var c in value)
                {
                    if(safe.Contains(c)) res += c.ToString();
                }
                SafeProjectName = res;
            }
        }

        public ProjectModel()
        {

        }

    }
}
