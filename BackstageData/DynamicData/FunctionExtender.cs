﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData
{
    internal static class FunctionExtender
    {
        public static string Extend(string Data, EngineFunction Extender)
        {
            var result = Data;
            switch (Extender)
            {
                case EngineFunction.None:
                    break;
                case EngineFunction.Password:
                    result = string.Format("PASSWORD({0})", Data);
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
