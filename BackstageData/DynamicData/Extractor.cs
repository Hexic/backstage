﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BackstageData
{
    internal static class Extractor
    {
        /// <summary>
        /// Extrai propriedades marcadas pelo ColumnAttribute
        /// </summary>
        /// <param name="T">Tipo a ser processado</param>
        /// <param name="includeKeyColumn">Indica que a KeyColumn deve fazer parte dos resultados ou não. Padrão: <c>true</c></param>
        /// <returns>Array de propriedades</returns>
        /// 
        public static string[] ExtractPropertiesName(Type T, bool includeKeyColumn = true, bool includeWriteOnlyColumns = false)
        {
            if (T.GetCustomAttributes(typeof(TableAttribute), false).Length != 1) throw new ArgumentException("O tipo T não implementa o atributo Table");
            List<string> props = new List<string>();
            foreach (var prop in T.GetProperties())
            {
                bool isColumn = (prop.GetCustomAttributes(typeof(ColumnAttribute), false).Length == 1);
                bool isKey = (prop.GetCustomAttributes(typeof(KeyColumnAttribute), false).Length == 1);
                bool isWriteOnly = (prop.GetCustomAttributes(typeof(WriteOnlyColumnAttribute), false).Length == 1);
                if (!isColumn && !isKey && !isWriteOnly) continue;
                if (isColumn || (isKey && includeKeyColumn) || (isWriteOnly && includeWriteOnlyColumns)) props.Add(prop.Name);
                else continue;
            }
            return props.ToArray();
        }

        /// <summary>
        /// Extrai o nome da coluna marcada como KeyColumn
        /// </summary>
        /// <param name="T">Tipo a ser processado</param>
        /// <returns>Nome da coluna ou null, caso ela não exista</returns>
        public static string ExtractKeyColumn(Type T)
        {
            if (T.GetCustomAttributes(typeof(TableAttribute), false).Length != 1) throw new ArgumentException("O tipo T não implementa o atributo Table");
            foreach (var prop in T.GetProperties())
            {
                var args = prop.GetCustomAttributes(typeof(KeyColumnAttribute), false);
                if (args.Length != 1) continue;
                else return prop.Name;
            }
            return null;
        }

        /// <summary>
        /// Verifica se um objeto faz parte do DynamicData
        /// </summary>
        /// <param name="graph">Objeto a ser analisado</param>
        /// <returns><c>true</c> se o objeto for válido</returns>
        public static bool IsValidGraph(object graph)
        {
            return IsValidGraph(graph.GetType());
        }
        
        /// <summary>
        /// Verifica se um objeto faz parte do DynamicData
        /// </summary>
        /// <param name="graph">Objeto a ser analisado</param>
        /// <returns><c>true</c> se o objeto for válido</returns>
        internal static bool IsValidGraph(Type graph)
        {
            var t = graph;
            bool hasTableAttr = t.GetCustomAttributes(typeof(TableAttribute), false).Length == 1;
            bool hasColumns = t.GetProperties().Any(c => c.GetCustomAttributes(typeof(ColumnAttribute), false).Length == 1);
            bool hasKey = t.GetProperties().Any(c => c.GetCustomAttributes(typeof(KeyColumnAttribute), false).Length == 1);

            return hasTableAttr && hasColumns && hasKey;
        }

        internal static PropertyInfo[] ExtractProperties(Type graph, bool includeKeyColumn = true)
        {
            var t = graph; 
            if (t.GetCustomAttributes(typeof(TableAttribute), false).Length != 1) throw new ArgumentException("O tipo T não implementa o atributo Table");
            var props = new List<PropertyInfo>();
            foreach (var prop in t.GetProperties())
            {
                bool isColumn = (prop.GetCustomAttributes(typeof(ColumnAttribute), false).Length == 1);
                bool isKey = (prop.GetCustomAttributes(typeof(KeyColumnAttribute), false).Length == 1);
                if (!isColumn && !isKey) continue;
                if (isColumn || (isKey && includeKeyColumn)) props.Add(prop);
                else continue;
            }
            return props.ToArray();
        }

        internal static PropertyInfo[] ExtractProperties(object graph, bool includeKeyColumn = true)
        {
            var t = graph.GetType();
            if (t.GetCustomAttributes(typeof(TableAttribute), false).Length != 1) throw new ArgumentException("O tipo T não implementa o atributo Table");
            var props = new List<PropertyInfo>();
            foreach (var prop in t.GetProperties())
            {
                bool isColumn = (prop.GetCustomAttributes(typeof(ColumnAttribute), false).Length == 1);
                bool isKey = (prop.GetCustomAttributes(typeof(KeyColumnAttribute), false).Length == 1);
                if (!isColumn && !isKey) continue;
                if (isColumn || (isKey && includeKeyColumn)) props.Add(prop);
                else continue;
            }
            return props.ToArray();
        }
    }
}
