﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace BackstageData
{
    public static class Store
    {
        /// <summary>
        /// Insere ou Atualiza um modelo no database
        /// </summary>
        /// <param name="graph">Modelo a ser inserido/atualizado</param>
        /// <returns>Código do modelo inserido / atualizado</returns>
        public static uint Insert(object graph)
        {
            if (!Extractor.IsValidGraph(graph)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var t = graph.GetType();
            var query = new StringBuilder();
            var key = Extractor.ExtractKeyColumn(t);
            var pro = Extractor.ExtractPropertiesName(t, false, true).ToList();

            bool isInsert = (t.GetProperty(key).GetValue(graph, null).ToString() == "0");

            if (isInsert)
            {

                query.Append("insert into ");
                query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
                query.Append(" (");
                query.Append(key);
                query.Append(", ");


                // Determinar valores nulos
                List<string> removable = new List<string>();
                foreach (string p in pro) if (t.GetProperty(p).GetValue(graph, null) == null) removable.Add(p);
                foreach (string s in removable) pro.Remove(s);


                query.Append(string.Join(", ", pro));
                query.Append(") values (");
                var param = new List<string>();
                query.Append("null");
                query.Append(", ");



                foreach (string p in pro)
                {
                    bool isWriteOnly = t.GetProperty(p).GetCustomAttributes(typeof(WriteOnlyColumnAttribute), false).Length == 1;
                    bool isCommon = t.GetProperty(p).GetCustomAttributes(typeof(ColumnAttribute), false).Length == 1;
                    EngineFunction E = EngineFunction.None;
                    E = (isWriteOnly ?
                        (t.GetProperty(p).GetCustomAttributes(typeof(WriteOnlyColumnAttribute), false)[0] as WriteOnlyColumnAttribute).ExtendFunction :
                        (t.GetProperty(p).GetCustomAttributes(typeof(ColumnAttribute), false)[0] as ColumnAttribute).ExtendFunction);


                    param.Add(string.Format(FunctionExtender.Extend("@{0}", E), p));
                }
                query.Append(string.Join(", ", param.ToArray()));
                query.Append(")");
                var Parameters = new List<MySqlParameter>();
                foreach (string p in pro) Parameters.Add(new MySqlParameter(string.Format("@{0}", p), t.GetProperty(p).GetValue(graph, null)));

#if DEBUG
                Console.WriteLine("Store.Insert para graph " + t.FullName + " -- INSERT");
                Console.WriteLine("Query:\r\n" + query.ToString());
                Console.WriteLine("Parameters:");
                foreach (MySqlParameter p in Parameters) Console.WriteLine("  - " + p.ParameterName + " = " + p.Value);
                Console.WriteLine("");
#endif


                MySqlConnection k = new MySqlConnection(Settings.sc);
                try
                {
                    uint ret = 0;
                    k.Open();
                    var com = k.CreateCommand();
                    com.CommandText = query.ToString();
                    com.Parameters.AddRange(Parameters.ToArray());
                    if (com.ExecuteNonQuery() == 1)
                    {
                        ret = uint.Parse(com.LastInsertedId.ToString());
                    }
                    k.Close();
                    return ret;
                }
                catch (Exception)
                {
                }
                finally
                { k.Close(); }
                return 0;
            }
            else
            {
                query.Append("update ");
                query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
                query.Append(" set ");
                var sets = new List<string>();

                // Determinar valores nulos
                List<string> removable = new List<string>();
                foreach (string p in pro) if (t.GetProperty(p).GetValue(graph, null) == null) removable.Add(p);
                foreach (string s in removable) pro.Remove(s);

                foreach (string p in pro)
                {
                    bool isWriteOnly = t.GetProperty(p).GetCustomAttributes(typeof(WriteOnlyColumnAttribute), false).Length == 1;
                    bool isCommon = t.GetProperty(p).GetCustomAttributes(typeof(ColumnAttribute), false).Length == 1;
                    EngineFunction E = EngineFunction.None;
                    E = (isWriteOnly ?
                        (t.GetProperty(p).GetCustomAttributes(typeof(WriteOnlyColumnAttribute), false)[0] as WriteOnlyColumnAttribute).ExtendFunction :
                        (t.GetProperty(p).GetCustomAttributes(typeof(ColumnAttribute), false)[0] as ColumnAttribute).ExtendFunction);
                    var v = (string.Format(FunctionExtender.Extend("@{0}", E), p));
                    sets.Add(string.Format("{0} = {1}", p, v));
                }
                query.Append(string.Join(", ", sets.ToArray()));
                query.Append(" where " + key + " = @" + key);
                var Parameters = new List<MySqlParameter>();
                foreach (string p in pro) Parameters.Add(new MySqlParameter(string.Format("@{0}", p), t.GetProperty(p).GetValue(graph, null)));
                Parameters.Add(new MySqlParameter("@" + key, t.GetProperty(key).GetValue(graph, null)));

#if DEBUG
                Console.WriteLine("Store.Insert para graph " + t.FullName + " -- UPDATE");
                Console.WriteLine("Query:\r\n" + query.ToString());
                Console.WriteLine("Parameters:");
                foreach (MySqlParameter p in Parameters) Console.WriteLine("  - " + p.ParameterName + " = " + p.Value);
                Console.WriteLine("");
#endif

                MySqlConnection k = new MySqlConnection(Settings.sc);
                try
                {
                    uint ret = 0;
                    k.Open();
                    var com = k.CreateCommand();
                    com.CommandText = query.ToString();
                    com.Parameters.AddRange(Parameters.ToArray());
                    if (com.ExecuteNonQuery() == 1)
                    {
                        ret = uint.Parse(com.LastInsertedId.ToString());
                    }
                    k.Close();
                    return uint.Parse(t.GetProperty(key).GetValue(graph, null).ToString());
                }
                catch (Exception)
                {
                }
                finally
                { k.Close(); }
                return 0;
            }
        }
        /// <summary>
        /// Carrega um modelo do database a partir de seu código
        /// </summary>
        /// <typeparam name="T">Tipo do modelo a ser carregado</typeparam>
        /// <param name="ID">ID do modelo a ser carregado</param>
        /// <returns>O modelo solicitado, ou null em caso de erro.</returns>
        public static T SelectSingle<T>(uint ID) where T : class
        {
            var t = typeof(T);
            StringBuilder query = new StringBuilder();
            if (!Extractor.IsValidGraph(t)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var pro = Extractor.ExtractPropertiesName(t);
            var key = Extractor.ExtractKeyColumn(t);
            query.Append("select * from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
            query.Append(" where " + key + " = " + ID);

#if DEBUG
            Console.WriteLine("Store.SelectSingle<" + t.FullName + ">(" + ID + ")");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                MySqlDataReader r = com.ExecuteReader();
                if (!r.HasRows)
                {
                    k.Close();
                    return null;
                }
                else
                {
                    r.Read();
                    var ret = Activator.CreateInstance(t);
                    foreach (var prop in Extractor.ExtractProperties(t))
                    {
                        prop.SetValue(ret, r[prop.Name], null);
                    }
                    r.Close();
                    k.Close();
                    return ret as T;
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
        /// <summary>
        /// Carrega todos os objetos T do database.
        /// </summary>
        /// <typeparam name="T">Modelo de objeto a ser carregado</typeparam>
        /// <returns>Array de modelos carregados</returns>
        public static T[] SelectMultiple<T>() where T : class
        {
            var t = typeof(T);
            StringBuilder query = new StringBuilder();
            if (!Extractor.IsValidGraph(t)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var pro = Extractor.ExtractPropertiesName(t);
            var key = Extractor.ExtractKeyColumn(t);
            query.Append("select * from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);

#if DEBUG
            Console.WriteLine("Store.SelectMultiple<" + t.FullName + ">");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                MySqlDataReader r = com.ExecuteReader();
                if (!r.HasRows)
                {
                    k.Close();
                    return new List<T>().ToArray();
                }
                else
                {
                    var list = new List<T>();
                    while (r.Read())
                    {
                        var ret = Activator.CreateInstance(t);
                        foreach (var prop in Extractor.ExtractProperties(t))
                        {
                            prop.SetValue(ret, r[prop.Name], null);
                        }
                        list.Add(ret as T);
                    }

                    r.Close();
                    k.Close();

                    return list.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return new List<T>().ToArray();
        }
        /// <summary>
        /// Carrega um único objeto T que corresponde à uma condição where.
        /// Esse método não aceita parametros. Utilize-o para comparar valores numéricos
        /// </summary>
        /// <typeparam name="T">Modelo a ser carregado</typeparam>
        /// <param name="where">Condição where a ser executada. Exempo: "x = y"</param>
        /// <returns>Objeto T que corresponde à condição, ou <c>null</c></returns>
        public static T SelectSingle<T>(string where) where T : class
        {
            var t = typeof(T);
            StringBuilder query = new StringBuilder();
            if (!Extractor.IsValidGraph(t)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var pro = Extractor.ExtractPropertiesName(t);
            var key = Extractor.ExtractKeyColumn(t);
            query.Append("select * from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
            query.Append(" where " + where);
            query.Append(" limit 1");

#if DEBUG
            Console.WriteLine("Store.SelectSingle<" + t.FullName + ">(" + where + ")");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                MySqlDataReader r = com.ExecuteReader();
                if (!r.HasRows)
                {
                    k.Close();
                    return null;
                }
                else
                {
                    r.Read();
                    var ret = Activator.CreateInstance(t);
                    foreach (var prop in Extractor.ExtractProperties(t))
                    {
                        prop.SetValue(ret, r[prop.Name], null);
                    }
                    r.Close();
                    k.Close();
                    return ret as T;
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
        /// <summary>
        /// Carrega uma array de objetos T que correspondem à uma condição where.
        /// Esse método não aceita parametros. Utilize-o para comparar valores numéricos.
        /// </summary>
        /// <typeparam name="T">Modelos a serem carregados</typeparam>
        /// <param name="where">Condição where a ser executada. Exempo : "x = y"</param>
        /// <returns>Array de objetos que correspondem à condição</returns>
        public static T[] SelectMultiple<T>(string where) where T : class
        {
            var t = typeof(T);
            StringBuilder query = new StringBuilder();
            if (!Extractor.IsValidGraph(t)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var pro = Extractor.ExtractPropertiesName(t);
            var key = Extractor.ExtractKeyColumn(t);
            query.Append("select * from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
            query.Append(" where " + where);

#if DEBUG
            Console.WriteLine("Store.SelectMultiple<" + t.FullName + ">");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                MySqlDataReader r = com.ExecuteReader();
                if (!r.HasRows)
                {
                    k.Close();
                    return new List<T>().ToArray();
                }
                else
                {
                    var list = new List<T>();
                    while (r.Read())
                    {
                        var ret = Activator.CreateInstance(t);
                        foreach (var prop in Extractor.ExtractProperties(t))
                        {
                            prop.SetValue(ret, r[prop.Name], null);
                        }
                        list.Add(ret as T);
                    }

                    r.Close();
                    k.Close();

                    return list.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return new List<T>().ToArray();
        }
        /// <summary>
        /// Carrega um único objeto T que corresponde à uma condição where.
        /// Esse método aceita parametros. Utilize-o para comparar qualquer tipo de dado.
        /// </summary>
        /// <typeparam name="T">Modelo a ser carregado</typeparam>
        /// <param name="where">Condição where a ser executada. Exemplo: "x = @y"</param>
        /// <param name="parameters">Parametros que compõem a condição</param>
        /// <returns>Objeto T wue corresponde à condição ou <c>null</c></returns>
        public static T SelectSingle<T>(string where, MySqlParameter[] parameters) where T : class
        {
            var t = typeof(T);
            StringBuilder query = new StringBuilder();
            if (!Extractor.IsValidGraph(t)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var pro = Extractor.ExtractPropertiesName(t);
            var key = Extractor.ExtractKeyColumn(t);
            query.Append("select * from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
            query.Append(" where " + where);
            query.Append(" limit 1");

#if DEBUG
            Console.WriteLine("Store.SelectSingle<" + t.FullName + ">(" + where + ", arr[])");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("Parameters:");
            foreach (MySqlParameter p in parameters) Console.WriteLine("  - " + p.ParameterName + " = " + p.Value);
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                com.Parameters.AddRange(parameters);
                MySqlDataReader r = com.ExecuteReader();
                if (!r.HasRows)
                {
                    k.Close();
                    return null;
                }
                else
                {
                    r.Read();
                    var ret = Activator.CreateInstance(t);
                    foreach (var prop in Extractor.ExtractProperties(t))
                    {
                        prop.SetValue(ret, r[prop.Name], null);
                    }
                    r.Close();
                    k.Close();
                    return ret as T;
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
        /// <summary>
        /// Carrega uma array de objetos T que correspondam à uma condição where
        /// Esse método aceita parametros. Utilize-o para comparar qualquer tipo de dado.
        /// </summary>
        /// <typeparam name="T">Modelos a serem carregados</typeparam>
        /// <param name="where">Condição where a ser executada. Exemplo "x = @y"</param>
        /// <param name="parameters">Paramoetros que compõem a condição</param>
        /// <returns>Array de objetos T que correspondem à condição</returns>
        public static T[] SelectMultiple<T>(string where, MySqlParameter[] parameters) where T : class
        {
            var t = typeof(T);
            StringBuilder query = new StringBuilder();
            if (!Extractor.IsValidGraph(t)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var pro = Extractor.ExtractPropertiesName(t);
            var key = Extractor.ExtractKeyColumn(t);
            query.Append("select * from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
            query.Append(" where " + where);

#if DEBUG
            Console.WriteLine("Store.SelectMultiple<" + t.FullName + ">(" + where + ", arr[])");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("Parameters:");
            foreach (MySqlParameter p in parameters) Console.WriteLine("  - " + p.ParameterName + " = " + p.Value);
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                com.Parameters.AddRange(parameters);
                MySqlDataReader r = com.ExecuteReader();
                if (!r.HasRows)
                {
                    k.Close();
                    return new List<T>().ToArray();
                }
                else
                {
                    var list = new List<T>();
                    while (r.Read())
                    {
                        var ret = Activator.CreateInstance(t);
                        foreach (var prop in Extractor.ExtractProperties(t))
                        {
                            prop.SetValue(ret, r[prop.Name], null);
                        }
                        list.Add(ret as T);
                    }

                    r.Close();
                    k.Close();

                    return list.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return new List<T>().ToArray();
        }
        /// <summary>
        /// Remove um modelo do database
        /// </summary>
        /// <param name="graph">Modelo a ser removido</param>
        /// <returns>Quantidade de modelos removidos</returns>
        public static uint Destroy(object graph)
        {
            if (!Extractor.IsValidGraph(graph)) throw new ArgumentException("Objeto informado não faz parte do conjunto DynamicData");
            var t = graph.GetType();
            var query = new StringBuilder();
            var key = Extractor.ExtractKeyColumn(t);
            var pro = Extractor.ExtractPropertiesName(t, false);
            query.Append("delete from ");
            query.Append((t.GetCustomAttributes(typeof(TableAttribute), false)[0] as TableAttribute).TableName);
            query.Append(string.Format(" where {0} = @{0}", key));

#if DEBUG
            Console.WriteLine("Store.Destroy<" + t.FullName + ">");
            Console.WriteLine("Query:\r\n" + query.ToString());
            Console.WriteLine("");
#endif

            try
            {
                MySqlConnection k = new MySqlConnection(Settings.sc);
                k.Open();
                MySqlCommand com = new MySqlCommand(query.ToString(), k);
                com.Parameters.AddWithValue(string.Format("@{0}", key), t.GetProperty(key).GetValue(graph, null));
                uint ret = uint.Parse(com.ExecuteNonQuery().ToString());
                k.Close();
                return ret;
            }
            catch (Exception)
            {
            }
            return 0;

        }
        /// <summary>
        /// Remove vários modelos do database
        /// </summary>
        /// <param name="graphs">Modelos a serem removidos</param>
        /// <returns>Quantidade de modelos removidos</returns>
        public static uint Destroy(params object[] graphs)
        {
            uint ret = 0;
            foreach (var graph in graphs) ret += Destroy(graph);
            return ret;
        }
    }
}