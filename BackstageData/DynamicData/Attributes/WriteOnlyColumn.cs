﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    sealed class WriteOnlyColumnAttribute : Attribute
    {
        public WriteOnlyColumnAttribute()
        {
        }

        private EngineFunction E = EngineFunction.None;
        public EngineFunction ExtendFunction
        {
            get { return E; }
            set { E = value; }
        }
    }
}
