﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    sealed class KeyColumnAttribute : Attribute
    {
        public KeyColumnAttribute()
        {
        }
    }
}
