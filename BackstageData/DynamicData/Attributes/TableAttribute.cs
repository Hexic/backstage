﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackstageData
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class TableAttribute : Attribute
    {
        readonly string tableName;

        public TableAttribute(string TableName)
        {
            this.tableName = TableName;
        }

        public string TableName
        {
            get { return tableName; }
        }

    }
}
